from flask import Flask
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)


def requiredData(name, id, token, all):
    users = [
        {'Name': 'Sainath Bingi', 'Id': '9999', 'Token': '5667547364239647'},
        {'Name': 'Bhargava', 'Id': '123', 'Token': '6555672369286375665'},
        {'Name': 'Gunasekhar', 'Id': '567', 'Token': '66476428946446'},
        {'Name': 'Nishant', 'Id': '456', 'Token': '664623686498326483'},
        {'Name': 'Edwin', 'Id': '254', 'Token': '5364876438296436678'}
    ]
    if all:
        return users

    if not name:
        for user in users:
            del user['Name']

    if not id:
        for user in users:
            del user['Id']

    if not token:
        for user in users:
            del user['Token']

    return users


class Data(Resource):
    def get(self, name, id, token, all):
        data = requiredData(name, id, token, all)
        return data


api.add_resource(Data, '/Details/<int:name>/<int:id>/<int:token>/<int:all>')


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000)
